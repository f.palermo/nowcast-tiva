# This script adds to the file nowcastTiva_v20.csv
# data on GVA and EMPL at sectoral level,
# data on consumer prices, producer prices, exchange rates, and interest rates at the country level, and
# data on Trade in Services (both import and export, in USD, towards rest of the world)

# input files are
# //main.oecd.org/em_sources/TIVA_NOWCAST/Data/concordance_table_section_division_ISIC.xlsx
# "//main.oecd.org/em_sources/TIVA_NOWCAST/Data/va_emp_trade_by_sector.xlsx (multiple sheets)
# V:/TIVA_NOWCAST/sources/Data/nowcastTiva_v20.csv

# output file is
# //main.oecd.org/em_sources/TIVA_NOWCAST/Data/draft_sectoral_nowcastTiva_Aug31.csv

packages <- c("gridExtra","stringi","tidyverse")
invisible(lapply(packages, function(x) {library(x, quietly=T, character.only=T)}))

# paths
path_to_new_data <- "//main.oecd.org/em_sources/TIVA_NOWCAST/Data"
path_to_economy_data <- file.path("V:","TIVA_NOWCAST","sources","Data")
path_to_MEI_dataset <- "V:\\TIVA_NOWCAST\\ICIO\\mei_data.rds"
output <- path_to_new_data

# obtain correspondence table ---------------------------------------------------------
# The aggregations we need are labelled as "Code"
# The table below has four columns: Section, Divisions (i.e. group of divisions in that section), Code, and divs_in_code (divisions in that Code, one by one in long format, useful for joins)
correspondence_disaggregated <- rio::import(file.path(path_to_new_data,"concordance_table_section_division_ISIC.xlsx"), sheet=2) %>% 
  rowwise() %>% 
  mutate(divs_in_code = ifelse(is.na(Code),NA,list(c(str_sub(Code,2,3):str_sub(Code,-2))))) %>% 
  ungroup() %>% 
  unnest_longer(divs_in_code) %>% 
  mutate(divs_in_code=as.character(divs_in_code)) %>% 
  filter(!is.na(Code))

section_code <- correspondence_disaggregated %>%
  distinct(Section,Code) %>% 
  mutate(Section=ifelse(!is.na(Section),
                        paste0("V",Section),
                        Section))

# import and tidy data (empl_alfs,empl_natacc,gva_cur,gva_const)
countries <- rio::import(file.path(path_to_new_data,"va_emp_trade_by_sector.xlsx"), sheet=6)         # list of Countries in our analysis

base_data <- countries %>% full_join(section_code, by=character()) %>% full_join(data.frame(year=c(1995:2021)), by=character()) %>% arrange(country,Code,year)

main_df <- rio::import(file.path(path_to_economy_data,"nowcastTiva_v21.csv"))                        # original dataset, will join sectoral data with it

empl_alfs <- rio::import(file.path(path_to_new_data,"va_emp_trade_by_sector.xlsx"), sheet=4) %>%     # employment from labor force survey
  select(-SID,-SEX,-FREQUENCY,country=LOCATION,sector=SUBJECT) %>% 
  filter(!grepl("994",sector)) %>%
  mutate(sector=paste0("V",str_sub(sector,5,5))) %>% 
  pivot_longer(cols=c(`1995`:`2021`), names_to="year", values_to="EMPL_PPL_ALFS")

empl_nat_acc <- rio::import(file.path(path_to_new_data,"va_emp_trade_by_sector.xlsx"), sheet=2) %>%  # employment from national accounts
  filter(MEASURE=="PER") %>% 
  select(-SID,-MEASURE,-TRANSACT,country=LOCATION,sector=ACTIVITY) %>% 
  pivot_longer(cols=c(`1995`:`2021`), names_to="year", values_to="EMPL_PPL_NATACC") %>% 
  mutate(sector=ifelse(sector=="VA0","VA",sector))

va_cur <- rio::import(file.path(path_to_new_data,"va_emp_trade_by_sector.xlsx"), sheet=1) %>%        # gross value added, current prices
  select(-SID,-TRANSACT,-MEASURE,country=LOCATION,sector=ACTIVITY) %>% 
  pivot_longer(cols=c(`1995`:`2021`), names_to="year", values_to="GVA_CUR_PR") %>% 
  mutate(sector=ifelse(sector=="VA0","VA",sector))

va_const<- rio::import(file.path(path_to_new_data,"va_emp_trade_by_sector.xlsx"), sheet=7) %>%       # gross value added, constant prices
  select(-SID,-TRANSACT,-MEASURE,country=LOCATION,sector=ACTIVITY) %>% 
  pivot_longer(cols=c(`1995`:`2021`), names_to="year", values_to="GVA_CONST_PR_oecd_base") %>% 
  mutate(sector=ifelse(sector=="VA0","VA",sector))

# import TiS data at sector level from BoP (EBOPS classification, to be matched with NACE Rev.2) and impute missing observations with B6CRSE01 ----
ebops_nace <- rio::import(file.path(path_to_new_data,"EBOPS-NACE.xlsx"),sheet=3) %>% 
  filter(Division != "D31T33") %>% 
  select(-comments,-Section) %>% 
  separate_rows(ebops,sep=",") %>% 
  group_by(Division) %>% 
  mutate(ebops=list(ebops)) %>% 
  ungroup() %>% 
  distinct() %>% 
  unnest_longer(ebops)                                                          # correspondence table

base_services <- section_code %>%
  select(-Section) %>%
  full_join(countries, by=character()) %>%
  filter(as.numeric(str_sub(Code,2,3))>=35) %>% 
  expand(country,Code) %>%
  arrange(country,Code) %>% 
  full_join(data.frame(year=c(1995:2021)), by=character()) %>% 
  rename(Division=Code)

tis_sectoral_base <- rio::import(file.path(path_to_new_data,"TiS.xlsx")) %>% 
  pivot_longer(cols=c(`1995`:`2021`), names_to='year') %>% 
  select(-SID,-MEASURE,-PAR) %>% 
  pivot_wider(names_from=EXP, values_from=value) %>% 
  rename(country=COU) %>% 
  inner_join(ebops_nace, by=c("SER"="ebops")) %>% 
  group_by(country,year,Division) %>% 
  summarise(across(c(EXP,IMP), ~ sum(.x, na.rm=T)),
            ebops=list(SER)) %>% 
  ungroup() %>% 
  mutate(across(c(EXP,IMP), na_if, 0),
         year=as.double(year)) %>%
  rename(TiS_EXP_div=EXP,
         TiS_IMP_div=IMP) %>% select(-ebops) %>%                                # these data need to be imputed
  complete(country,Division,year) %>% 
  arrange(country,Division,year)

tis_sectoral <- base_services %>%
  left_join(tis_sectoral_base, by=c("country","Division","year"))               # explicit missing codes and countries in the table

# impute sectoral TiS data with economy-wide B6CRSE01
tis_sectoral_imputed <- tis_sectoral %>%
  left_join(main_df %>% select(country,year,B6CRSE01,B6DBSE01), by=c("country","year")) %>% 
  group_by(country,Division) %>% 
  arrange(country,Division,year) %>% 
  mutate(across(c(contains("TiS"),B6CRSE01,B6DBSE01), ~ ifelse(is.na((.x/lag(.x,n=1))),1,(.x/lag(.x,n=1))), .names="{.col}_var")) %>% 
  mutate(EXP_div = ifelse(!is.na(TiS_EXP_div/lag(TiS_EXP_div,n=1)),TiS_EXP_div_var,B6CRSE01_var),
         IMP_div = ifelse(!is.na(TiS_IMP_div/lag(TiS_IMP_div,n=1)),TiS_IMP_div_var,B6DBSE01_var)) %>% 
  mutate(across(c(EXP_div,IMP_div), ~ 100*cumprod(.x), .names="{.col}_imputed")) %>% 
  select(country,Division,year,TiS_EXP_div=EXP_div_imputed,TiS_IMP_div=IMP_div_imputed)

# join new data
new_data <-  va_cur %>% 
  full_join(va_const, by=c("country","sector","year")) %>% 
  full_join(empl_alfs, by=c("country","sector","year")) %>% 
  full_join(empl_nat_acc, by=c("country","sector","year")) %>% 
  rename(sector_from=sector) %>% 
  filter(country %in% countries$country) %>% 
  mutate(sector_from=case_when(grepl("_",sector_from) ~ paste0("D",str_sub(sector_from,2,3),"T",str_sub(sector_from,5,6)), # e.g. change V10_12 into D10T12
                               sector_from=="VTOT" ~ sector_from,                                                          # leave the row for "total" as it is
                               str_sub(sector_from,2,2) %in% LETTERS ~ str_sub(sector_from,1,2),                           # e.g. change "VA" (section) into "A"
                               TRUE ~ paste0("D",str_sub(sector_from,2,3))))                                              # e.g. change V46 (division) into 46
#rm(va_cur,va_const,empl_alfs,empl_nat_acc)

# split data: sector level, division level (whether aggregated or not) -----------------
new_data_sec <- new_data %>%                                                    # keep only data at section level
  filter(str_sub(sector_from,2) %in% c(LETTERS,"TOT")) %>% 
  rename(GVA_CUR_PR_sec=GVA_CUR_PR,
         GVA_CONST_PR_oecd_base_sec=GVA_CONST_PR_oecd_base,
         EMPL_PPL_ALFS_sec=EMPL_PPL_ALFS,
         EMPL_PPL_NATACC_sec=EMPL_PPL_NATACC) %>% 
  filter(!is.na(GVA_CUR_PR_sec) | !is.na(GVA_CONST_PR_oecd_base_sec)| !is.na(EMPL_PPL_ALFS_sec) | !is.na(EMPL_PPL_NATACC_sec)) %>%  # and drop rows where all variables are missing
  left_join(section_code, by=c("sector_from"="Section"))

new_data_divlevel <- new_data %>%                                                                    # keep only data at division level (whether single divisions or groups of divisions)
  anti_join(new_data_sec, by=c("country","sector_from","year")) %>% 
  select(-EMPL_PPL_ALFS) %>% 
  filter(!is.na(GVA_CUR_PR) | !is.na(GVA_CONST_PR_oecd_base)| !is.na(EMPL_PPL_NATACC)) %>%           # and drop rows where all variables are missing
  mutate(sector_from=ifelse(stri_length(sector_from)<=4,
                            paste0("D",str_sub(sector_from,2,3)),
                            paste0("D",str_sub(sector_from,2,3),"T",str_sub(sector_from,5,6))))

# data are not always available at the same level of aggregation as we need (i.e. classification in column "Code" above)
# some match, some don't, and the latter will need some work
# first, separate divisions/aggregations that match our classification from unmatched ones ----
new_data_divlevel_compatible <- new_data_divlevel %>% 
  rename(Code=sector_from) %>% 
  semi_join(correspondence_disaggregated, by=c("Code")) %>% 
  mutate(missing_divisions=list(""))

# obtain a list of divisions/groups of divisions at which data is available ------------
sectors_available <- new_data_divlevel %>%                                                           # make a vector of all aggregations at which data are available, will be used for a join below
  distinct(sector_from)

# construct a correspondence table between the aggregation levels at which data are
# available and the aggregation level that we need (and could not match immediately as above)
unfilled_codes <- correspondence_disaggregated %>% 
  anti_join(new_data_divlevel_compatible, by="Code") %>%                                             # drop all Codes to which we could immediately assign the corresponding data
  distinct(Code) %>% 
  full_join(sectors_available, by = character()) %>%                                                 # attach available aggregations to our desired aggregation: this is a full join, so it does not make sense per se, as it requires suitable filtering (lines below) to keep only correct matches
  rowwise() %>% 
  mutate(groups_to=list(as.numeric(str_sub(Code,2,3)):as.numeric(str_sub(Code,-2))),                 # create a new column "groups_to" containing, for each row, a list of the divisions in the corresponding row in Code: e.g. in correspondence with D10T12 we will have a list 10,11,12 
         groups_from=list(as.numeric(str_sub(sector_from,2,3)):as.numeric(str_sub(sector_from,-2))), # same with the column reporting the aggregation at which data are available
         excess_sectors=list(setdiff(groups_from,groups_to))) %>%                                    # create a new column "excess sectors" containing, for each row, the list of divisions that are in "groups_from" but not in "groups_to": we do not want to match these data together, because it would mean associating one or more divisions to a group of divisions that they do not belong to
  ungroup() %>% 
  filter(excess_sectors=="integer(0)") %>%                                                           # so we filter those observations out. In this way, "groups_from" is contained in "groups_to": note that the rows in groups_to can contain more divisions than in groups from. This is why we will summarise by groups_to over groups_from, making sure to avoid double-counting any division
  select(-excess_sectors)

new_data_divlevel_other <- unfilled_codes %>% 
  left_join(new_data_divlevel %>% select(country,year,sector_from), by=c("sector_from")) %>%         # once the correspondence "groups_from"-"groups_to" is obtained, joined the data that we need to aggregate at the desired aggregation level
  ungroup() %>% 
  rowwise() %>% 
  mutate(start_of_groups_from=groups_from %>% first(),
         end_of_groups_from=groups_from %>% last()) %>% 
  ungroup() %>% 
  group_by(country,year,Code,groups_to) %>% 
  arrange(start_of_groups_from,desc(end_of_groups_from)) %>%
  mutate(cumul_groups_from=accumulate(.x=groups_from,.f=union) %>% as.list() %>% lag(n=1)) %>%       # we now want to avoid double-summing: to this end, we create a new column reporting, for each country-year-Code (Code is the target aggregation level), all the sectors appeared in groups_from in the previous rows. We do this by iterating the function union by means of accumulate. If a number (i.e. division) is present in the same row both in groups_from and in the new column, this means that it has already appeared at least once before, so we would double-sum at least one division if we consider that row.
  ungroup() %>% 
  rowwise() %>% 
  mutate(does_overlap=list(groups_from %in% cumul_groups_from)) %>%                                  # define a logical variable taking value TRUE whenever there is one or more overlaps, based on the column just created above
  #ungroup() %>% 
  #group_by(country,Code,year,groups_to) %>%
  #arrange(start_of_groups_from,desc(end_of_groups_from)) %>% 
  #select(-start_of_groups_from, -end_of_groups_from) %>% 
  #ungroup() %>% rowwise() %>% 
  filter(!TRUE %in% does_overlap) %>%                                                                # filter out observations with any overlaps to avoid double-counting
  ungroup() %>% 
  inner_join(new_data_divlevel, by=c("country","year","sector_from")) %>%                            # now that our correspondence between available aggregations and desired aggregation is complete and without overlapping, join the actual data
  group_by(country,year,Code,groups_to) %>%                                                          # now group by country, year, and Code (keep groups_to just for convenience, it defines the same grouping as Code)
  summarise(across(c(GVA_CUR_PR,GVA_CONST_PR_oecd_base,EMPL_PPL_NATACC), ~ sum(.x, na.rm=TRUE)),     #then sum the values
            groups_from=list(reduce(.x=groups_from,.f=union))) %>%                                   # and, for each country-year-Code, keep track of the divisions that were actually summed. This is meant to keep track of missing/unavailable data data: missing some observations leads to underestimating the value of the aggregation
  ungroup() %>% 
  rowwise() %>% 
  mutate(missing_divisions=ifelse(length(list(groups_to[!groups_to %in% groups_from]) %>% unlist())==0, # use the divisions reported in groups_from to create a new column "missing_divisions" that reports any divisions that are nominally in the Code, but the corresponding data is not available
                                  list(""),
                                  list(groups_to[!groups_to %in% groups_from]))) %>% 
  ungroup() %>% 
  select(-groups_to,-groups_from)

# bind datasets
df_division <- new_data_divlevel_compatible %>%                                                      # now bind data at division level just obtained with the ones that could be obtained immediately because the aggregations did match
  bind_rows(new_data_divlevel_other) %>% 
  mutate(across(c(GVA_CUR_PR,GVA_CONST_PR_oecd_base,EMPL_PPL_NATACC), ~ na_if(.x,0)))                # assuming that, if an observation is 0, it is missing, make NA all observations that are equal to zero

# add MEI and TiS variables at country level -------------------------------------------
# Import and prepare MEI dataset
# The issue here is just about variable selection. There are multiple variables (and measures of the latter)
# The criterion is to choose as few variable as possible to cover all the country-years as made possible by MEI dataset
# As for exchange rates and consumer prices, one variable covers all country-years covered by MEI
# As for producer prices and interest rates, 2 and 3 variables (respectively) are selected, where the first is the first best, and the others are used to cover the country-years not covered by the first best
# Please note that several country-years are not covered anyway, so we will need a solution to it (imputation of course, but it might not be enough)
mei_data <- readRDS(path_to_MEI_dataset) %>% 
  filter(subject %in% c("PIEAMP01","PITGND01", #PPI (PI)
                        "PISPFG01", # PPI (state of production) (PISP)
                        # Currency exchange (CC) #"CCRETT01" removed
                        "CPHPTT01", # CPI harmonized prices (CPHP)
                        "IRLTLT01","IRSTCB01") # interest rates (IR) #"IR3TIB01" removed
  ) %>% 
  filter(measure=="IXOB" | grepl("IR",subject)) %>% 
  filter(country %in% countries$country) %>% 
  filter(frequency=="Y") %>% 
  group_by(country,subject,measure,year) %>% 
  filter(versionno==max(versionno)) %>% 
  ungroup() %>% 
  select(-versionno,-status,-frequency,-frequency_fct,-period,-segmentno,-measure,-name_e) %>% 
  pivot_wider(names_from=subject, values_from=value) %>% 
  select(country,year,PIEAMP01,PISPFG01,PITGND01,CPHPTT01,IRLTLT01,IRSTCB01)

# Import and prepare TiS dataset
# tis <- rio::import(file.path(path_to_new_data,"va_emp_trade_by_sector.xlsx"), sheet=8) %>%
#   filter(COU %in% countries$country & SER=="S") %>%
#   select(-SID,-SER,-MEASURE,-PAR) %>%
#   pivot_longer(cols=c(`1995`:`2021`), names_to="year", values_to="value") %>%
#   pivot_wider(names_from=EXP, values_from=value) %>%
#   rename(TiS_EXP_USD_WLD=EXP,
#          TiS_IMP_USD_WLD=IMP,
#          country=COU) %>% 
#   mutate(year=as.numeric(year)) %>% 
#   group_by(country) %>% 
#   fill(contains("TiS"), .direction="downup") %>% 
#   ungroup()'

# join MEI + TiS data together with main dataset at country level ----------------------
df_economy <- mei_data %>% 
  # full_join(tis, by=c("country","year")) %>% 
  full_join(main_df, by=c("country","year"))

# join dataset at division level with data at section level
df_sectoral <- df_division %>% 
  left_join(section_code, by="Code") %>% 
  complete(country,nesting(Section,Code),year) %>% 
  full_join(new_data_sec, by=c("country","year","Section"="sector_from","Code")) %>%                        # join data at section level
  mutate(year=as.integer(year)) %>% 
  select(country,Section,Code,missing_divisions,year,GVA_CUR_PR_sec,GVA_CONST_PR_oecd_base_sec,EMPL_PPL_ALFS_sec,EMPL_PPL_NATACC_sec,GVA_CUR_PR,GVA_CONST_PR_oecd_base,EMPL_PPL_NATACC) %>% 
  right_join(base_data, by=c("country","Section","Code","year")) %>%                                        # add rows with missing values
  left_join(tis_sectoral_imputed, by=c("country","Code"="Division","year")) %>% 
  arrange(country,Section,Code,year)

# join new data at economy level + those at sector level to main dataset
df_final <- df_sectoral %>% 
  full_join(df_economy, by=c("country","year"))

# export dataset
rio::export(df_final,file.path(output,"draft_sectoral_nowcastTiva_Sep22.rds"))

# check data quality (quality of imputation)
plots_tis_sectoral <- tis_sectoral_imputed %>% 
  pivot_longer(cols=contains("TiS")) %>% 
  group_by(country,Division) %>% 
  nest() %>% 
  mutate(countrycode=paste0(country,".",Division),
         plot=map2(data,countrycode, ~ ggplot(data=.x, aes(x=year, y=value, group=name, color=name)) +
                     geom_line(size=1.3) +
                     labs(title=.y)))

ggsave(
  filename=file.path("C:","Users","palermo_f","LocalData","Nowcasting","plots_tis_sectoral.pdf"),
  plot=marrangeGrob(plots_tis_sectoral$plot, nrow=2, ncol=2),
  width=15, height=9
)